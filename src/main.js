// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
require("vuetify/dist/vuetify.min.css");
require("vue-toastr/dist/vue-toastr.min.css");

import Vue from "vue"
import App from "./App"
import router from "./router"
import Vuetify from "vuetify"
import axios from "axios"
import Vuex from "vuex"
import store from "./store"
import auth from "./packages/auth.js"
import VueCookie from "vue-cookie"
import "font-awesome/css/font-awesome.min.css"
import VeeValidate, {Validator} from "vee-validate"
import plValidation from "vee-validate/dist/locale/pl"
import Toastr from "vue-toastr"
import Moment from "vue-moment"
import { Laue } from 'laue';

Vue.use(Vuetify, {
    iconfont: "fa4"
});
Vue.use(Vuex);
Vue.use(auth);
Vue.use(VueCookie);
Validator.localize({pl: plValidation});
Vue.use(VeeValidate, {locale: "pl"});
Vue.use(Toastr);
Vue.use(Moment);
Vue.use(Laue);

Vue.prototype.$http = axios;
axios.defaults.baseURL = "https://churchmanagerbackend.azurewebsites.net/api/";
axios.defaults.headers.common["Authorization"] = "Bearer " + Vue.auth.getToken();

const UNAUTHORIZED = 401;
axios.interceptors.response.use(
    response => response,
    error => {
        const {status} = error.response;
        if (status === UNAUTHORIZED) {
            Vue.auth.refreshToken();
        }
        return Promise.reject(error);
    }
);

Vue.config.productionTip = false;

router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.forVisitors)) {
            if (Vue.auth.getUserRole()) {
                next({
                    path: "/"
                })
            } else next();
        }

        else if (to.matched.some(record => record.meta.forAdmins)) {
            if (Vue.auth.getUserRole() !== "Admin") {
                next({
                    path: "/"
                })
            } else next();
        }
        else next();
    }
);
/* eslint-disable no-new */
new Vue({
    el: "#app",
    router,
    store,
    components: {App},
    template: "<App/>"
});
