import Vue from "vue"
import Router from "vue-router"
import Homepage from "../components/Homepage/Homepage"
import ResetPassword from "../components/ResetPassword"
import Events from "../components/Events/Events"
import Announcements from "../components/Announcements/Announcements"
import AboutUs from "../components/AboutUs/AboutUs"
import AdminPanel from "../components/AdminPanel/AdminPanel"
import Login from "../components/Login"
import Charts from "../components/Statistics/Charts"

Vue.use(Router);

export default new Router({
    mode: "history",

    routes: [
        {
            path: "/",
            name: "Homepage",
            component: Homepage
        },
        {
            path: "/wydarzenia",
            name: "Events",
            component: Events
        },
        {
            path: "/ogloszenia",
            name: "Announcements",
            component: Announcements
        },
        {
            path: "/o-nas",
            name: "AboutUs",
            component: AboutUs
        },
        {
            path: "/logowanie",
            name: "Login",
            component: Login,
            meta: {
                forVisitors: true
            }
        },
        {
            path: "/resetuj-haslo",
            name: "ResetPassword",
            component: ResetPassword,
            props: (route) => ({query: route.query.token}),
            meta: {
                forVisitors: true
            }
        },
        {
            path: "/admin",
            name: "AdminPanel",
            component: AdminPanel,
            meta: {
                forAdmins: true
            }
        },
        {
            path: "/statystyki",
            name: "Charts",
            component: Charts,
            meta: {
                forAdmins: true
            }
        },
        {
            path: "*",
            redirect: "/",
        }
    ]
})
