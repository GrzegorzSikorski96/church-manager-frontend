import cookie from "vue-cookie"
import axios from "axios"
import store from "../store"
import router from "../router"

export default function (Vue) {
    Vue.auth = {
        async refreshToken() {
            let refreshToken = this.getRefreshToken();
            if (refreshToken) {
                await axios.post("Authentication/token/refresh", {refreshToken: refreshToken})
                    .then(response => {
                        let token = response.data;
                        this.setTokens(refreshToken, token);
                        let authData = this.getAuthData();
                        store.commit("passAuthData", JSON.parse(authData));
                        axios.defaults.headers.common["Authorization"] = "Bearer " + token;
                    }) .catch(error => {
                        this.destroyUserData();
                        router.push({name: "Login"});
                        store.commit("passAuthData", null);
                    })
            } else {
                this.destroyUserData();
                store.commit("passAuthData", null);
            }
        },

        setTokens(refreshToken, token) {
            cookie.set("refresh_token", refreshToken, "1d");
            cookie.set("token", token, "1d");
        },

        getToken() {
            return cookie.get("token");
        },

        getRefreshToken() {
            return cookie.get("refresh_token");
        },

        destroyUserData() {
            cookie.delete("token");
            cookie.delete("refresh_token");
            cookie.delete("auth_data");
        },

        checkIfAuthTokenExists() {
            return this.getToken();
        },

        setAuthData(authData) {
            cookie.set("auth_data", JSON.stringify(authData), "1d");
        },

        getAuthData() {
            return cookie.get("auth_data");
        },

        getUserRole() {
            if (cookie.get("auth_data")) {
                return JSON.parse(cookie.get("auth_data")).roles[0];
            }
            else {
                return null;
            }
        }
    };

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get: () => {
                return Vue.auth
            }
        }
    })
}